# HIFIS Materials

This project contains HIFIS materials.

## HIFIS Overview Download

The compiled HIFIS overviews can be downloaded from:

* https://gitlab.hzdr.de/hifis/overall/communication/hifis-news/-/jobs/artifacts/main/browse/build?job=build

## Compile the PDF

To build the overview run `make` or rely on GitLab CI:

```bash
make
```
