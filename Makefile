SUBDIRS := $(wildcard */*/.)

all: $(SUBDIRS)
$(SUBDIRS):
		$(MAKE) -C $@

.PHONY: all $(SUBDIRS)

clean:
	rm -r build
